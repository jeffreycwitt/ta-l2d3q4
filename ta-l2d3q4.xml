<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="https://bitbucket.org/lombardpress/lombardpress-schema/raw/master/LombardPressODD.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://bitbucket.org/lombardpress/lombardpress-schema/raw/master/LombardPressODD.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Librum II, Distinctio 3, Quaestio 4</title>
        <author>Thomas Aquinas</author>
        <editor/>
        <respStmt>
          <name>Jeffrey C. Witt</name>
          <resp>TEI Encoder</resp>
        </respStmt>
      </titleStmt>
      <editionStmt>
        <edition n="2015.09-dev-master">
          <title>Librum II, Distinctio 3, Quaestio 4</title>
          <date when="2016-03-16">March 16, 2016</date>
        </edition>
      </editionStmt>
      <publicationStmt>
        <publisher/>
        <pubPlace/>
        <availability status="free">
          <p>Public Domain</p>
        </availability>
        <date when="2016-03-16">March 16, 2016</date>
      </publicationStmt>
      <sourceDesc>
        <p>1856 editum</p>
      </sourceDesc>
    </fileDesc>
    <revisionDesc status="draft">
      <listChange>
        <change when="2015-09-06" who="#JW" status="draft" n="2015.09">
          <note type="validating-schema">lbp-0.0.1</note>
        </change>
      </listChange>
    </revisionDesc>
  </teiHeader>
  <text xml:lang="la">
    <front>
      <div xml:id="includeList">
        <xi:include
          href="https://bitbucket.org/lombardpress/lombardpress-lists/raw/master/workscited.xml"
          xpointer="worksCited"/>
        <xi:include
          href="https://bitbucket.org/lombardpress/lombardpress-lists/raw/master/Prosopography.xml"
          xpointer="prosopography"/>
      </div>
      <div xml:id="starts-on"> </div>
    </front>
    <body>
      <div xml:id="ta-l2d3q4">
        <head>Librum II, Distinctio 3, Quaestio 4</head>
        <div type="articulus">
          <head>Articulus 1</head>

          <head type="questionTitle">Utrum Angelus in statu suo naturali dilexerit Deum plus quam se
            et omnia alia</head>

          <p>Circa tertium principale quaeritur de dilectione, scilicet utrum Angeli si in gratia
            creati non sunt, in illo statu innocentiae Deum super se et plus quam omnia dilexerunt.
            Et videtur, quod non. Sic enim Deum diligere est actus caritatis, qui caritatem non
            habentis esse non potest. Sed Angeli in isto statu caritatem non habebant quae sine
            gratia non est, cum nunquam sit informis. Ergo videtur quod non dilexerunt Deum plus
            quam se.</p>
          <p>Praeterea, secundum Bernardum natura semper in se curva est. Sed dilectio Angelorum in
            primo statu non fuit nisi ex principio naturali. Ergo tota in amantem reflectebatur, ut
            quidquid diligerent Angeli, propter seipsos diligerent; et ita non Deum supra se
            diligebant.</p>
          <p>Praeterea, Avicenna dicit, quod nulla actio alicujus naturae est liberalis, nisi solius
            Dei, ex hoc quod omnem actionem aliam sequitur aliquod commodum ipsius agentis. Sed
            dilectio qua aliquis Deum super se diligit, est maxime liberalis. Ergo videtur quod hoc
            non possit alicui convenire nisi per gratiam.</p>
          <p>Praeterea, unumquodque naturaliter appetit acquirere suum finem. Sed omne quod
            desideratur ab aliquo ut sibi acquirendum, propter se et minus se diligitur. Cum ergo
            Angeli in statu naturalium Deum sicut suum finem dilexerint, videtur quod eum super se
            non dilexerunt.</p>
          <p>Praeterea, secundum Dionysium, bona naturalia in Angelis remanent etiam post peccatum.
            Sed constat quod peccator homo vel Angelus non diligit Deum super omnia. Ergo videtur
            quod nec in statu naturalium.</p>
          <p>Sed contra, omnis dilectio aut est usus, aut fruitionis. Si ergo Angeli diligebant Deum
            in statu innocentiae, aut ut utentes aut ut fruentes. Si ut utentes, ergo utebantur re
            fruenda, quod est magnae perversitatis, secundum Augustinum; et ita esset dilectio
            peccati, et non naturalis. Si ut fruentes, ergo Deum propter seipsum diligebant, quia
            frui est amore inhaerere alicui rei propter seipsam.</p>
          <p>Praeterea, Deum esse super omnia diligendum, cum sit legis naturalis, scriptum erat in
            mente Angeli multo expressius quam in mente hominis. Sed non contingit sine peccato
            facere contra id quod per naturalem legem cordi inditum est. Ergo si Deum super omnia
            non diligebant, peccabant. Quamdiu ergo sine peccato fuerunt, Deum super omnia
            dilexerunt.</p>
          <p>Respondeo dicendum, quod circa hoc est duplex opinio. Quidam enim distinguunt
            dilectionem concupiscentiae et amicitiae: quae duo si diligenter consideremus, differunt
            secundum duos actus voluntatis, scilicet appetere, quod est rei non habitae, et amare,
            quod est rei habitae, secundum Augustinum. Est ergo dilectio concupiscentiae qua quis
            aliquid desiderat ad concupiscendum, quod est sibi bonum secundum aliquem modum; et tali
            dilectione Angeli in statu naturalium Deum plus quam se diligebant vehementius appetendo
            bonum divinum quam suum: quia hoc erat eis delectabilius; unde totam hanc dilectionem in
            seipsos retorquebant. Dilectio autem amicitiae est qua aliquis aliquid, vel
            similitudinem ejus quod in se habet, amat in altero volens bonum ejus ad quem
            similitudinem habet: et propter hoc philosophus dicit quod est similis a simili amari,
            sicut unus virtuosus alium diligit; in quibus tamen est vera amicitia; et tali
            dilectione Angeli quodammodo diligebant Deum supra se: quia ei majus bonum quam sibi
            optabant, scilicet esse Deum, quod sibi non volebant; sed aliquod minus bonum; sed tamen
            intensius sibi volebant bonum creatum quam Deo bonum divinum. Sed ista responsio non
            potest stare: oportet enim ponere Angelos ad Deum habuisse dilectionem amicitiae, cum
            secundum bona naturalia similitudo divina in eis resplenderet. Hoc autem est de ratione
            amicitiae quod quamvis habeat dilectiones et utilitates annexas, non tamen ad has oculus
            amantis respicit, sed ad bonum amatum. Ergo in corde amantis praeponderat bonum amatum
            omnibus utilitatibus vel dilectionibus quae consequuntur ex amato. Sed nullum bonum erat
            in Angelo quod non esset ex ipso amato, scilicet Deo. Ergo plus diligebant bonum amatum
            divinum quam bonum quod ipsi erant, vel quod in eis erat; et haec est alia opinio.</p>
          <p>Ad primum ergo dicendum, quod actus caritatis potest dici dupliciter: vel qui est ex
            caritate; et hoc non est nisi in habente caritatem: vel qui est ad caritatem, non sicut
            meritorius vel generativus, sed sicut praeparativus; et sic actus caritatis ante
            caritatem habitam haberi potest, sicut facere justa est ante habitum justitiae. Vel
            potest dici, quod in amicitia caritatis movetur animus ad amandum Deum ex similitudine
            gratiae; sed in dilectione naturali ex ipso boni naturae, quod etiam est similitudo
            summae bonitatis. Et propter hoc dicendum, quod cum dicitur, quod habens caritatem
            diligit Deum propter se ipsum, ly propter denotat habitudinem finis et efficientis, quia
            ipse Deus superaddit naturae unde in ejus dilectionem tendit; sed cum dicitur de carente
            caritate, quod diligit Deum propter se ipsum, ly propter denotat habitudinem finis, et
            non efficientis proximi, nisi sicut Deus in omni natura operante operatur.</p>
          <p>Ad secundum dicendum, quod natura in se curva dicitur, quia semper diligit bonum suum.
            Non tamen oportet quod in hoc quiescat intentio quod suum est, sed in hoc quod bonum
            est: nisi enim sibi esset bonum aliquo modo, vel secundum veritatem, vel secundum
            apparentiam, nunquam ipsum amaret. Non tamen propter hoc amat quia suum est; sed quia
            bonum est: bonum enim est per se objectum voluntatis.</p>
          <p>Ad tertium dicendum, quod quamvis cuilibet naturae creatae agenti ex sua actione
            proveniat aliquod commodum, non tamen oportet quod illud commodum sit intentum; sicut
            patet in amicitia honestorum.</p>
          <p>Ad quartum dicendum, quod finis est duplex: quidam proportionatus rei, receptus in ipsa
            ut perfectio inhaerens sibi, sicut sanitas in operationibus medicinae; et quia iste
            finis non est secundum esse nisi in eo cui acquiritur, ideo absolute non amatur ab
            aliquo supra se; sed se esse sub tali perfectione, amatur supra se esse simpliciter. Sed
            est quidam finis per se subsistens non dependens secundum esse a re quae est ad finem;
            et iste finis desideratur quidem acquiri; sed amatur supra id quod acquisitum est ab
            illo; et talis finis est Deus, ut supra dictum est.</p>
          <p>Ad quintum dicendum, quod bona naturalia, prout in esse naturae absolute considerantur,
            remanent integra post peccatum, tamen pervertuntur quantum ad rectum ordinem quem
            habebant in gratia vel virtute; et hanc rectitudinem consequebatur, super omnia Deum
            diligere.</p>
        </div>
        <div>
          <head>Expositio textus</head>
          <p><quote>Hic quaeri solet quam sapientiam habuerunt ante casum vel
            confirmationem</quote>. Ponitur hic duplex Angelorum cognitio; una speculativa, qua
            cognoscebant Deum et seipsos et alias creaturas; et alia practica sequens eorum
            operationem, quae ad eos pertinebat, qua cognoscebant quid eligere et respuere deberent.
            Et haec erat similis cognitioni prudentiae in nobis.</p>
        </div>

      </div>
    </body>
  </text>
</TEI>
